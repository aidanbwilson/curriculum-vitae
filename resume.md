Mission statement
---------

<hr>

<p align="center">
My mission is to enable researchers to change our world through the use of technology; to remove technical barriers to scientific discovery; and to build stronger research communities through collaboration.
</p>

Education
---------

<hr>

2010-2013
:   **Master of Arts (Research), Linguistics**; The University of Melbourne  
    Thesis title: *Tiwi Revisited: A reanalysis of Traditional Tiwi verb morphology*  
    \(<http://hdl.handle.net/11343/39640>\)

2001-2006
:   **Bachelor of Liberal Studies**; The University of Sydney  
    Honours (Class I) in Linguistics  
    Thesis title: *Negative evidence in Linguistics: The case of Wagiman complex predicates*  
    \(<http://hdl.handle.net/2123/5385>\)  
    Majors: Linguistics, History and Philosophy of Science

Employment
----------

<hr>

2016-Current
:   **IT Relationship Manager, Research, and eResearch Analyst**; Australian Catholic University

    - Member of IT Strategy and Planning team
    - Member of service performance and project portfolio management group, overseeing and coordinating all IT projects
    - Responsible for managing relationship between ACU IT, ODVC(R), and all research capabilities
    - Technical and business adviser to IT senior management on matters relating to eResearch, including policy and code compliance, sector trends and practices at other universities
    - Technical and business adviser to Research Office and all ACU researchers, including:
        - Data collection, management, retention, disposal and publishing
        - Use of appropriate eResearch tools such as HPC, cloud computing, survey platforms
        - Researcher training in tools and techniques
    - Administrator of ACU's:
        - CloudStor tenant portal
        - AAF Virtual Home dashboard
        - REDCap instance
        - Research Git hosting instance
    - Liaison and negotiaion with vendors of research software such as QSR (NVivo) and Qualtrics
    - Member of eResearch Working Party incorporating IT, ODVC(R) and the Library, with activities including:
        - Coordinating and aligning eResearch activities across the university
        - Reviewing relevant internal policies, such as the Research Data Management policy
        - Fielding enquiries regarding access to restricted government datasets
        - Advising on data storage, retention and disposal practices in line with relevant internal and external policies and codes
        - Ideating and delivering on business initiatives to benefit the ACU Research community

2015-Current
:   **eResearch Analyst**; Intersect Australia

    - eResearch Analyst for Australian Catholic University (2016-current)
        - Functioning as ACU IT Business Relationship Manager for Research
    - eResearch Analyst for Macquarie University and Western Sydney University (2015-2016)
    - Coordinator of Intersect Learning & Development program
        - Managing a team of eResearch trainers
        - Designing and developing programs and systems to manage training logistics, course evaluation and reporting
        - Accredited Software Carpentry instructor
        - Member of Software Carpentry Foundation Advisory Council
    - Developing reporting systems for member universities, encompassing HPC usage, cloud compute usage, data storage, engineering projects and training attendance
    - Various capacities besides eResearch Analyst: Data Analyst, Business Analyst, Project Manager and Account Manager
    - Various duties/experience in managing staff, client liaison, technical writing and documentation, developing training courses, reporting both internally and to external clients, developing business cases, leading business projects, strategic planning
    - Engagement and negotiation with external organisations, including NCI, CSIRO, ANDS, NeCTAR, RDS, AARNet, AAF, NCRIS capabilities, medical research institutes and government agencies

2011-2014
:   **Archive Systems Officer**; PARADISEC

    Responsibilities:

    - Solution design and implementation
    - Developing and maintaining archive systems
    - Training staff in operations and workflows
    - Curating collection data and metadata
    - Liaison with depositors and other clients and contacts

2006-2010
:   **Audio Systems Officer**; PARADISEC

    Responsibilities:

    - Digitising analogue audio material
    - Overseeing deposits and archive processes
    - Data and metadata management
    - Liason with depositors

2014-2015
:   **Research Assistant**; Mediating the Converstion, Korean News Project

    Responsibilities:  

    - Developing and maintaining a web data harvesting system to generate research data  
    - Deploying and maintaining virtual hardware  
    - Research data management including offsite backups and recovery protocols  
    - Developing methodologies for the analysis of data according to the project aims and research questions  

2007-2010
:   **Tutor**; Department of Linguistics, The University of Sydney

    Tutoring and lecturing six courses over five semesters in introductory and advanced linguistics courses.  
    Responsibilities:

    - Designing and editing teaching materials, assessments and exams
    - Grading assessments and exams
    - Providing student feedback
    - Managing online learning materials and facilitating online portion of course delivery

Research
--------------

<hr>

2010-2014
:   **Aboriginal Child Language Acquisition project**; The University of Melbourne.  
Australian Research Council funded project aimed at studying the linguistic behaviour of Aboriginal children from multilingual communities.  
*Field site:* Tiwi Islands, Northern Territory, Australia.  

2010-2013
:   **Traditional Tiwi documentation project**; The University of Melbourne.  
Awarded research grant from AIATSIS (Australian Institute for Aboriginal and Torres Strait Islander Studies) to undertake documentation of the Traditional Tiwi language.  

2009
:   **Project for Free Electronic Dictionaries (PFED)**; The University of Sydney.  
Research project to create mobile dictionary applications for minority languages including Australian Aboriginal languages.  
Funded by a grant from the Hoffman Foundation.  

2005-2008
:   **Documentation of the Wagiman Language**; The University of Sydney.  
Involvement in an Australian Research Council funded project to document several langauges from Australia's top-end.  

Awards
----------------------------------------

<hr>

2011  
:   **Traditional Tiwi Documentation Project**; Australian Institute for Aboriginal and Torres Strait Islander Studies (AIATSIS) research grant G7667/2011.  
  

2010  
:   **Melbourne Research Scholarship**; The University of Melbourne, postgraduate award.  
  

2009  
:   **Project for Free Electronic Dictionaries (PFED)**; The Hoffman Foundation.  

Publications
----------------------------------------

<hr>

Book chapters
:   Wilson, A., Hurst, P., & Wigglesworth, G. (2017). "Code switching or code mixing? Tiwi children’s use of language resources in a multilingual environment". In  G. Wigglesworth, J. Simpson, and J. Vaughan (eds.) *Language practices of Indigenous Children. The transition from Home to School*. Palgrave Macmillan \(doi: 10.1057/978-1-137-60120-9_6\)
  
:   Wilson, A. (2008) "Electronic dictionaries for language reclamation". In J.Hobson, K. Lowe, S. Poetsch, and M. Walsh (eds.) *Re-awakening languages: theory and practice in the revitalisation of Australia’s Indigenous languages*, pp339-48. Sydney: Sydney University Press. \(<http://hdl.handle.net/2123/6936>\)  
  
Theses
:   Wilson, A. (2013). *Tiwi Revisited: A reanalysis of Traditional Tiwi verb morphology*, Masters thesis. Melbourne: The University of Melbourne. \(<http://hdl.handle.net/11343/39640>\)  
  
:   Wilson, A. (2006). *Negative evidence in Linguistics: The case of Wagiman complex predicates*, Honours thesis. Sydney: The University of Sydney. \(<http://hdl.handle.net/2123/5385>\)  

Datasets
:   Wilson, A. (2012). *Traditional Tiwi field recordings, 2012 (AW1)*, Digital collection managed by PARADISEC. \(doi: 10.4225/72/56E97956B1EC9\)  

:   Wilson, A. (2005-07). *Aidan Wilson - Wagiman language recordings*, 47 sound recordings. AIATSIS Call No.: WILSON_A01.

References
----------

<hr>

&nbsp;
:   **Associate Professor Jonathan Arthur**  
    National Services Manager, Intersect  
    Phone: 0430 690 502  
    Email: jonathan\@intersect.org.au  

:   **Rod Lewis**  
    Senior Manager, Research Systems, ACU  
    Phone: 02 9739 2502  
    Email: rod.lewis\@acu.edu.au  

:   **Professor Linda Barwick**  
    Director, PARADISEC  
    Phone: 02 9351 1383  
    Email: linda.barwick\@sydney.edu.au  

:   **David Toll**  
    Former National Services Manager, Intersect  
    Phone: 0419 997 066  
    Email: dbtol\@hotmail.com  
