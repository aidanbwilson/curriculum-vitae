---
layout: post
title: Managing secure passwords
---
Managing passwords is something that is getting more and more complex. Large sites are often the target of attacks and some of these attacks expose hundreds or thousands of users' personal details. In the last few years I can recall major attacks on Ashley Madison, Yahoo, (a university in the US), and the US Department of ...

Of course, your personal security cannot prevent the hacking of services that you use, but you can at least limit your exposure to attacks by maintaining secure passwords. 

In the last couple of years I have moved from using a single, moderately strong password for all servics, to branching out and creating unique, somewhat strong, but still easily memorable, passwords for different services, particularly for those services that are in some way linked to finances, including Paypal, Ebay, my bank, and any other service that has my credit card or Paypal details. Doing so raises an obvious problem: How do you remember all your passwords?

There are several cloud-based password managers on the market that take care of this for you. These include Lastpass, Onepass, Dashlane and probably a myriad other services and applications. Of these, Lastpass is probably the most popular. Basically, these services and applications store your logins and passwords in an encrypted file which you unlock with a master password whenever you need to unlock a service, at which point you simply copy the password from the application, and paste it into the password field. They often include additional features like automatic locking of your database, clearing of your clipboard, reminders for password resetting, and very good password generators. In short, password managers allow you to create very secure, very long passwords that would take a computer many years to crack by brute force, but with no additional cognitive load on you to remember anything apart from your master password.

One important aspect of these services is whether they operate as online cloud-based services, and therefore store your passwords on a centralised server, or are offline, desktop applications that store your passwords in a local file. If you use a service to store your passwords in the cloud, then you take on some risk, as these services are themselves targets for attackers, although they invest heavily in security, so this may be an acceptable risk to you.

My preferred system uses the KeePass standard. KeePass is an open-source password database protocol with a bunch of applications built around it, including applications for Mac, Windows, Linux, and apps for iOS and Android. There is also a command-line application called kpcli which I have also begun using recently.

I have a friend who works in internet security, and she uses OnePass, but never allows her password database to touch the internet, in case the database is compromised. I allow myself the slight concession in security, and store my password database on a cloud data storage platform, which proives me with the convenience of having my password database automatically synced across all devices when I update it. As the KeePass database is encrypted with AES, so as long as I keep my master password secure, I can be relatively safe in the knowledge that even with the password database file, an attacker won't be able to see my passwords.

## 'Security' Questions

It seems that just when people are getting better about their own security, the services that we need to secure make things even easier for attackers. So-called security questions are a case in point. These are the questions about you that you don't have to remember as much as intrinsically know and can answer over the phone when talking with, for example, your bank if you need to confirm your identity. The problem is that the sorts of answers they need are especially easy for an even mildly adept social engineer to ascertain. Even a Facebook friend can fairly easily find out your mother's maiden name or the town where you were born. And often the security questions can be used to access an account instead of a password. 

So how can you make your account secure if your bank, for example, forces you to nominate security questions? Easy. Use a password manager and treat security questions as further passwords, i.e., with unguessable, and factually incorrect information, so that a social engineer can't find it out. Services like LastPass and OnePass will allow you to enter notes in entries. You can simply enter the security question text and the 'answer' that you specified for it in a note. A KeePass database has optional additional fields that you can define, so you can enter the question text as the attribute and the answer as the value. So, rather than saying your mother's maiden name is Smith, say that it is 'troubleshoot'. 

Sometimes a service will allow you to define your own swecurity question. This is even better. You can use a service like a random adjective-noun generator (such as [this one](http://creativityforyou.com/combomaker.html)) to generate pairs. Enter the first word as the security question, and the second as its answer. 

## 5l0ppy r41n570rm

If you want to get really secure, you could use passprhases instead. Passphrases are phrases made up of dictionary words that are in combination easy to remember but extremely difficult for brute force attackers to crack. This idea was popularised by Randall Munroe in his xkcd comic