---
layout: post
title: Building a book index with Python
---

It so happened that my partner had reason to build an index for a book she is publishing. She decided not to pay the publisher to do so because of the expense, and our naive assumption that it wouldn't be that difficult. 

Parenthetical note: It's not trivial. If you are asked if you want to write your own index or pay your publisher, weigh up how much 20+ hours of your time is actually worth to you.

Being relatively competent in programming and scripting, I thought it should be simple enough to use programs like pdfgrep and some core utilities to generate an alphabetically-sorted list of references and locators (page numbers). Turns out it was definitely not as simple as I had thought, and that professional indexers do some incredible work.

That said, if you want to do this yourself, here are some tips, and a pointer to a python program that I wrote that can assist you in some of the more repetitive, mindless tasks. You will still need to do a lot of the manual, analytical tasks though, and there is no getting around that. What I mean is, you will need to think about how the concepts in your book relate to one another and how they should be structured, then you will have to go through your book and identify where those
concepts occur  and not through something as unsophisticated as a text search)
