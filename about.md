---
layout: page
title: About
display: True
image: /Users/aidan/Dropbox/Documents/Personal/ProfileShot_2015.jpg 
permalink: /about/
---
This site chronicles my continual self-education about research technology as well as my perpetual interests in cycling, geekery, nerdism, photography, cooking, and general 

## About me

I am a research support professional from Sydney, working for [Intersect], a not-for-profit eResearch organisation that works closely with its network of member institutions to enable researchers to take advantage of technology.

My own background in research is in documentary field linguistics, specifically Australia's aboriginal languages. My Master's thesis is a study on the verb morphology of Traditional Tiwi, a polysynthetic language from the Tiwi Islands in Australia's top-end. Throughout my research I have also maintained an interest, mostly for professional reasons, on data management, field recording equipment, and technologies that facilitate and enable research and scholarship. As these interests grew, they gradually became expertise, and I began assisting colleagues and others with technology.

In 2006 I started working with PARADISEC, an archive of digital material relating to cultures of the Australian and Pacific region, initially in the capacity of an audio officer, in which I was responsible for producing archival-quality digital artefacts from analogue audio materials, and later in the capacity of an archive systems officer, a role in which I helped develop the network infrastructure and software stack that underpinned the archival workflow and business processes.

Since 2015 I have been an active member of the Australian eResearch support services sector, as an eResearch Analyst at Intersect.

[Intersect]: http://intersect.org.au
