**Att: Gabrielle Whelan**  
     Director, Strategic Engagement and Planning  
     Information and Communications Technology  
     The University of Sydney  

25 March, 2018

Dear Gabrielle,

Please find enclosed my application for the role of Associate Director, Research Technology, at the University of Sydney.

I am a researcher and technologist with more than a decade of experience in eResearch, and a passionate advocate for the use of technology in research.

As a descriptive linguist working with Australia's Aboriginal languages, I was an early adopter of eResearch methodologies. Concurrently, in my nine years at PARADISEC, I developed an interest in helping researchers use technology to overcome barriers in their work. My interests in technology and research and a creativity toward solution design has led me to my current role at Intersect, as eResearch Analyst for the Australian Catholic University, where I fulfil the position of IT Business Relationship Manager for Research.

My ability to communicate between technologists and researchers is my strongest asset. Being able to learn complex technological concepts and relate them to researchers, and to build rapport with researchers from a range of disciplines and work effectively and meaningfully with them, has enabled me to succeed in various simultaneous, challenging roles in two organisations.

My work at Intersect and ACU has demanded skills in client management, technical and business analysis, thought leadership, and strategic thinking, and has seen me engage with clients and stakeholders representing the full breadth and depth of the eResearch landscape and tertiary sector.

Having served almost three and a half years at Intersect, I am ready for the next challenge in my career, and this role presents a challenge that aligns closely to my talents, and my interests.

With my passion for eResearch, my technological acuity, and my demonstrated skill in managing complex relationships, I am a strong candidate for this position.


Sincerely,

![](signature-blue.png){width=200px}

Aidan Wilson

